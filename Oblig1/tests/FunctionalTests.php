<?php
/** Functional tests for the IMT2571 Assignment #1 application.
 *
 * The tests are using the Behat Mink for interfacing the web server.
 * @author Rune Hjelsvold
 * @see http://mink.behat.org/en/latest/
 */

namespace NO\NTNU\IMT2571\Assignment1\Controller;

use PDO;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;
// Included because of the documentation
use PHPUnit_Extensions_Database_DataSet_IDataSet;
use PHPUnit_Extensions_Database_DB_IDatabaseConnection;

require_once("vendor/autoload.php");
// Please set the proper test setup values for your system in TestProps.php
require_once("TestProps.php");

/**
 * Class for testing the functionality of an Assignment #1 implementation
 * in IMT2571.
 * @author Rune Hjelsvold
 */
class FunctionalTests extends TestCase
{
    use TestCaseTrait
    {
        setup as protected traitSetup;
    }


    /**
     * Index of the collection page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const COLLECTION_PAGE_TITLE_IDX = 0;

    /**
     * Index of the details page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const DETAILS_PAGE_TITLE_IDX = 1;

    /**
     * Index of the error page title in the $PAGE_TITLES array
     * @see $PAGE_TITLES 
     */
    const ERROR_PAGE_TITLE_IDX = 2;

    /**
      * only instantiate pdo once for test clean-up/fixture load
      * @var PDO
      * @static
      */
    protected static $pdo = null;

    /**
      * only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
      * @var PHPUnit_Extensions_Database_DB_IDatabaseConnection
      * @static
      */
    private $conn = null;
    
    /**
     * Constant for an invalid id - can be used to test operations called on non-existing books
     */    /**
     * Constant array for holding page titles
     * @see $PAGE_TITLES 
     * @static
     */
    protected static $PAGE_TITLES = array(
                        'Book Collection',
                        'Book Details',
                        'Error Page'
    );
    
    /**
      * only instantiate the testcases array once
      * @var array[mixed]
      * @static
      */
    protected static $testcases = null;

    /**
     * Holds the root URL for the Oblig 1 site.
     */
    protected $baseUrl = TEST_BASE_URL;
    
    /**
     * The Mink Session object.
     * @see 
     */
    protected $session;

    /**
     * Returns an array of test case books. The array should always be in synch with the
     * XML fixture file.
     * @return Book[] Array of test case books.
     * @static
     */
    protected static function getTestBooks()
    {
        if (!self::$testcases) {
            self::$testcases = array(
                array(
                    'id' => 1,
                    'title' => 'Jungle Book',
                    'author' => 'R. Kipling',
                    'description' => 'A classic book.'
                    ),
                array(
                    'id' => 2,
                    'title' => 'Moonwalker',
                    'author' => 'J. Walker',
                    'description' => null
                ),
                array(
                    'id' => 3, 
                    'title' => 'PHP & MySQL for Dummies',
                    'author' => 'J. Valade',
                    'description' => 'Written by some smart gal.'
                )
            );
        }
        return self::$testcases;
    }
    
    /**
     * Returning the PDO object for the database to be used for tests.
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO(
                    'mysql:dbname=' . TEST_DB_NAME . ';host=' . TEST_DB_HOST,
                    TEST_DB_USER, TEST_DB_PWD,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
                );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, TEST_DB_NAME);
        }

        return $this->conn;
    }
    
    /**
     * Returning the fixtures.
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__ . '/' . FIXTURE_XML_FILE);
    }

    /**
     * Initiates the testing session
     */
    protected function setup()
    {
        $driver = new \Behat\Mink\Driver\GoutteDriver();
        $this->session = new \Behat\Mink\Session($driver);
        $this->session->start();
        $this->traitSetup();
    }
  
    /**
     * Computes the number of books in the listed collection.
     * @param DocumentElement $page the web page containing the book list, or the site root page
     *        if no page reference is passed.
     * @return integer the number of books listed on the page.
     */
    protected function getBookListLength($page = null)
    {
        if (!$page) {
            $this->session->visit($this->baseUrl);
            $page = $this->session->getPage();
        }
        return sizeof($page->findAll('xpath', '//table[@id="bookList"]/tbody/tr'));
    }
        
    /**
     * Checks if the page is the expected one - based on page title.
     * @param DocumentElement $page The root element of page to be checked.
     * @param integer $expectedIdx The index of the expected page - within the
     *                $PAGE_TITLE array
     * @param string $bookAuthor book author
     * @static
     * @see $PAGE_TITLES
     */
    protected static function isExpectedPage($page, $expectedIdx)
    {
        $title = null;
        $titleEl = $page->find('xpath', 'head/title');
        
        // Page has a title element
        if ($titleEl) {
            $title = $titleEl->getText();
        }
        
        // Compare title to the expected value
        return $title === self::$PAGE_TITLES[$expectedIdx];
    }
    
    /**
     * Adds a book to the collection. The id of the new created book is stored
     * in $testBookId for cleanup purposes.
     * @param array[mixed] $bookData Data about the book to be added to the collection; associative array with keys 
     *                               'id', 'title', 'author', and 'description'. id will be set when the book is
     *                               added to the collection.
     */
    protected function addBook(&$bookData)
    {
        // Load book list to get to the addForm
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();
        $addForm = $page->find('xpath', '//form[@id="addForm"]');
        
        // Complete and submit addForm
        $addForm->find('xpath', 'input[@name="title"]')->setValue($bookData['title']);
        $addForm->find('xpath', 'input[@name="author"]')->setValue($bookData['author']);
        $addForm->find('xpath', 'input[@name="description"]')->setValue($bookData['description']);
        $addForm->submit();
        
        $page = $this->session->getPage();

        $newBook = $page->find('xpath', '//*[@id="newBook"]');
        if ($newBook) {
            // Retrieve the id from the book list page
            $bookData['id'] = $newBook->find('xpath', './/*[@id="newBookId"]')->getText();
        }
    }
        
    /**
     * Removes a book entry from the web site collection
     * @param integer $bookId id of the book to be removed from the collection.
     */
    protected function deleteBook($bookId)
    {
        // Load page details page, which contains the form to be used to delete the book entry
        $this->session->visit($this->baseUrl . '?id=' . $bookId);
        $page = $this->session->getPage();
        
        // Submit the delete form
        $delForm = $page->find('xpath', '//form[@id="delForm"]');
        $delForm->submit();
    }
    
    /**
     * Modifies data about a book in the collection
     * @param array[mixed] $bookData Data about the book to be modified; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     */
    protected function modifyBook($bookData)
    {
        // Load page containing form to modify the book entry
        $this->session->visit($this->baseUrl . '?id=' . $bookData['id']);
        $page = $this->session->getPage();

        // Complete and submit the form to modify the book entry
        $modForm = $page->find('xpath', '//form[@id="modForm"]');
        $modForm->find('xpath', 'input[@name="title"]')->setValue($bookData['title']);
        $modForm->find('xpath', 'input[@name="author"]')->setValue($bookData['author']);
        $modForm->find('xpath', 'input[@name="description"]')->setValue($bookData['description']);
        $modForm->submit();
    }

    /**
     * Asserts that data about a book in the collection matches the passed book.
     * Also asserts that the data matches the data on the book details page.
     * @param array[mixed] $bookData Data about the book to be verified; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     * @uses assertBookDetails()
     */
    protected function assertBookListEntry($bookData)
    {
        // Load book list to get to the book entries
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Find the book entry and verify the data matches the expected value
        $bookNode = $page->find(
            'xpath',
            '//table[@id="bookList"]/tbody/tr[@id="book' . $bookData['id'] . '"]'
        );
        $this->assertNotNull($bookNode);
        $this->assertEquals(
            $bookData['id'],
            $bookNode->find('xpath', 'td[1]/a')->getText(),
            'Unexpected id'
        );
        $detailsLink = $bookNode->find('xpath', 'td[1]/a');
    
        $this->assertEquals(
            $bookData['title'],
            $bookNode->find('xpath', 'td[2]')->getText(),
            'Unexpected title'
        );
        $this->assertEquals(
            $bookData['author'],
            $bookNode->find('xpath', 'td[3]')->getText(),
            'Unexpected author'
        );
        $this->assertEquals(
            $bookData['description'],
            $bookNode->find('xpath', 'td[4]')->getText(),
            'Unexpected description'
        );
        
        // Further verify that the content is the same on the details page
        $this->assertBookDetails($detailsLink, $bookData);
    }

    /**
     * Asserts that data about a book matches the data displayed on the book details page.
     * @param NodeElement $detailsLink The hyperlink element for the details page.
     * @param array[mixed] $bookData Data about the book to be verified; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     */
    protected function assertBookDetails($detailsLink, $bookData)
    {
        // Load book details page
        $detailsLink->click();
        $page = $this->session->getPage();

        // Verify values shown on form
        $modForm = $page->find('xpath', '//form[@id="modForm"]');
        $this->assertEquals(
            $bookData['id'],
            $modForm->find('xpath', 'input[@name="id"]')->getValue(),
            'Unexpected book id'
        );
        $this->assertEquals(
            $bookData['title'],
            $modForm->find('xpath', 'input[@name="title"]')->getValue(),
            'Unexpected book title'
        );
        $this->assertEquals(
            $bookData['author'],
            $modForm->find('xpath', 'input[@name="author"]')->getValue(),
            'Unexpected book author'
        );
        $this->assertEquals(
            $bookData['description'],
            $modForm->find('xpath', 'input[@name="description"]')->getValue(),
            'Unexpected book description'
        );
    }
    
    /**
     * Attempts to add a book - asserting it to be successful
     * @param array[mixed] $bookData Data about the book to be added; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     */
    protected function attemptSuccessfulAddBook($bookData)
    {
        $bookListLength = $this->getBookListLength();
        $this->addBook($bookData);

        // Verifying book content in book list and on book details page
        $this->assertEquals(
            $bookListLength + 1,
            $this->getBookListLength(),
            'Expecting that a new book was added to collection'
        );
        $this->assertBookListEntry($bookData);
    }

    /**
     * Attempts to add a book - asserting it to be unsuccessful
     * @param array[mixed] $bookData Data about the book to be added; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     */
    protected function attemptUnsuccessfulAddBook($bookData)
    {
        $this->addBook($bookData);

        // Verifying that error page is returned
        $page = $this->session->getPage();
        $this->assertTrue(
            self::isExpectedPage($page, self::ERROR_PAGE_TITLE_IDX),
            'Expecting error page'
        );
    }
    
    /**
     * Attempts to modify a book - asserting it to be unsuccessful
     * @param array[mixed] $bookData Data about the book to be modified; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
    */
    public function attemptSuccessfulModifyBook($bookData)
    {
        $this->modifyBook($bookData);
        
        // Verifying that book information has not been changed
        $this->assertBookListEntry($bookData);
    }
    
    /**
     * Attempts to modify a book - asserting it to be unsuccessful
     * @param array[mixed] $bookData Data about the book to be modified; associative array with keys  'id', 'title',
     *                               'author', and 'description'.
     */
    public function attemptUnsuccessfulModifyBook($bookData)
    {
        $this->modifyBook($bookData);
        
        // Verifying that book information is left unchanged
        $this->assertBookListEntry(self::getTestBooks()[0]);
    }
    
    /**
     * Test that the structure of book details page is as expected
     */
    public function testBookDetailsPage()
    {
        // Load book details page for the first book in the test database
        $this->session->visit($this->baseUrl . '?id=' . self::getTestBooks()[0]['id']);
        $page = $this->session->getPage();

        // Verifying page title
        $this->assertTrue(
            self::isExpectedPage($page, self::DETAILS_PAGE_TITLE_IDX),
            'Expecting details page'
        );

        // Verifying the form for modifying book data
        $form = $page->find('xpath', '//form[@id="modForm"]');
        $this->assertNotNull($form, 'Expecting modForm to be present');
        $op = $form->find('xpath', 'input[@name="op"]');
        $this->assertNotNull($op, 'Expecting operation attribute');
        $this->assertEquals('mod', $op->getValue(), 'Expecting mod operation to be present');

        // Verifying the form for deleting book entries
        $form = $page->find('xpath', '//form[@id="delForm"]');
        $this->assertNotNull($form, 'Expecting delForm to be present');
        $op = $form->find('xpath', 'input[@name="op"]');
        $this->assertNotNull($op, 'Expecting operation attribute');
        $this->assertEquals('del', $op->getValue(), 'Expecting del operation to be present');
    }
    
    /**
     * Test that the structure of book collection page is as expected.
     * @depends testBookDetailsPage
     */
    public function testBookCollectionPage()
    {
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Verifying that a collection page was returned
        $this->assertTrue(
            self::isExpectedPage($page, self::COLLECTION_PAGE_TITLE_IDX),
            'Expecting collection page'
        );

        // Verifying the presence of the form for adding new books
        $addForm = $page->find('xpath', '//form[@id="addForm"]');
        $this->assertNotNull($addForm, 'Expecting addForm to be present');

        // Verifying the presence of the operator for adding new books
        $addOp = $addForm->find('xpath', 'input[@name="op"]');
        $this->assertNotNull($addOp, 'Expecting operation attribute');
        $this->assertEquals('add', $addOp->getValue(), 'Expecting add operation input field');
        
        // Verifying that all the books in the test database are showing up in the book list
        foreach (self::getTestBooks() as $bookData) {
            $this->assertBookListEntry($bookData);
        }
    }
    
    
    /**
     * Tests that adding a book with ordinary field contents succeeds.
     * @depends testBookCollectionPage
     */
    public function testAddOrdinary()
    {
        $bookData = array(
            'id' => null,
            'title' => 'Test title',
            'author' => 'Test author',
            'description' => 'Test description'
        );
        $this->attemptSuccessfulAddBook($bookData);
    }
    
    /**
     * Tests that adding a book where fields contain single quotes succeeds.
     * @depends testBookCollectionPage
     */
    public function testAddContainsSingleQuote()
    {
        $bookData = array(
            'id' => null,
            'title' => "Test 'title'",
            'author' => "Test 'author'",
            'description' => "Test 'description'"
        );
        $this->attemptSuccessfulAddBook($bookData);
    }
    
    /**
     * Tests that adding a book where fields contain HTML special characters succeeds.
     * @depends testBookCollectionPage
     */
    public function testAddContainsSpecialHtmlChars()
    {
        $bookData = array(
            'id' => null,
            'title' => '<script document.body.style.visibility="hidden" />',
            'author' => '<script document.body.style.visibility="hidden" />',
            'description' => '<script document.body.style.visibility="hidden" />'
        );
        $this->attemptSuccessfulAddBook($bookData);
    }
    
    /**
     * Tests that adding a book with an empty title field fails.
     * @depends testBookCollectionPage
     */
    public function testAddEmptyTitle()
    {
        $bookData = array(
            'id' => null,
            'title' => '',
            'author' => 'Test author',
            'description' => 'Test description'
        );
        $this->attemptUnsuccessfulAddBook($bookData);
    }
    
    /**
     * Tests that adding a book with an empty author field fails.
     * @depends testBookCollectionPage
     */
    public function testAddEmptyAuthor()
    {
        $bookData = array(
            'id' => null,
            'title' => 'Test title',
            'author' => '',
            'description' => 'Test description'
        );
        $this->attemptUnsuccessfulAddBook($bookData);
    }
    
    /**
     * Tests that modifying a book succeeds for ordinary field contents.
     * @depends testBookDetailsPage
     */
    public function testModifyOrdinary()
    {
        // Choose first book in the test database as the target for the update
        $bookData = array(
            'id' => self::getTestBooks()[0]['id'],
            'title' => 'New title',
            'author' => 'New author',
            'description' => 'New description'
        );
        $this->attemptSuccessfulModifyBook($bookData);
    }
    
    /**
     * Tests that modifying a book succeeds when fields contain single quotes.
     * @depends testBookDetailsPage
     */
    public function testModifyContainsSingleQuote()
    {
        // Choose first book in the test database as the target for the update
        $bookData = array(
            'id' => self::getTestBooks()[0]['id'],
            'title' => "Test 'title'",
            'author' => "Test 'author'",
            'description' => "Test 'title'"
        );
        $this->attemptSuccessfulModifyBook($bookData);
    }
    
    /**
     * Tests that modifying a book succeeds when fields contain HTML special characters.
     * @depends testBookDetailsPage
     */
    public function testModifyContainsSpecialHtmlChars()
    {
        // Choose first book in the test database as the target for the update
        $bookData = array(
            'id' => self::getTestBooks()[0]['id'],
            'title' => '<script document.body.style.visibility="hidden" />',
            'author' => '<script document.body.style.visibility="hidden" />',
            'description' => '<script document.body.style.visibility="hidden" />'
        );
        $this->attemptSuccessfulModifyBook($bookData);
    }
    
    /**
     * Tests that modifying a book fails when title is empty.
     * @depends testBookDetailsPage
     */
    public function testModifyEmptyTitle()
    {
        // Choose first book in the test database as the target for the update
        $bookData = array(
            'id' => self::getTestBooks()[0]['id'],
            'title' => '',
            'author' => 'Test author',
            'description' => 'Test description'
        );
        $this->attemptUnsuccessfulModifyBook($bookData);
    }
    
    /**
     * Tests that modifying a book fails when author is empty.
     * @depends testBookDetailsPage
     */
    public function testModifyEmptyAuthor()
    {
        // Choose first book in the test database as the target for the update
        $bookData = array(
            'id' => self::getTestBooks()[0]['id'],
            'title' => 'Test title',
            'author' => '',
            'description' => 'Test description'
        );
        $this->attemptUnsuccessfulModifyBook($bookData);
    }
    
    /**
     * Tests deleting a book record from the collection.
     * @depends testBookDetailsPage
     */
    public function testBookDelete()
    {
        // Find number of books in collection prior to delete
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();
        $bookListLength = $this->getBookListLength($page);

        // Choose first book in the test database as the target for the delete
        $id = self::getTestBooks()[0]['id'];
        $this->deleteBook($id);
        $this->session->visit($this->baseUrl);
        $page = $this->session->getPage();

        // Verifying that book is removed from book list
        $this->assertEquals(
            $bookListLength-1,
            $this->getBookListLength($page),
            'Expecting book list to shrink in length'
        );
        $bookNode = $page->find('xpath', '//table[@id="bookList"]/tbody/tr[@id="book' . $id . '"]');
        $this->assertNull($bookNode, 'Expecting book to be removed from book list');
    }
    
    /**
     * Tests if book details page is open for SQL injection - i.e., if non-numeric
     * characters are accepted in id parameter
     * @depends testBookDetailsPage
     */
    public function testSqlInjection()
    {
        // Choose first book in the test database as the target for the injection attempt
        $id = self::getTestBooks()[0]['id'];
        $this->session->visit($this->baseUrl . '?id=' . $id . "'; drop table books;--");
        $page = $this->session->getPage();

        // Verifying that request was rejected
        $this->assertTrue(
            self::isExpectedPage($page, self::ERROR_PAGE_TITLE_IDX),
            'Expecting error page'
        );
    }
}
